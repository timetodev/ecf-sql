#titre Pour utiliser ce fichier SQL, il vous faudrat     

        Un Phpmyadmin   (un serveur MYSQL) 
        Symfony en version 4 au minimum     

#titre Pour importer la base de données allez dans Phpmyadmin:

    Cliquer sur importer  
        ![noelshack.com](https://image.noelshack.com/fichiers/2019/08/5/1550847283-screen1-import.png)
    Selectionné choisir un fichier
       ![noelshack.com](https://www.noelshack.com/2019-08-5-1550847283-screen2-select.png)
    Selectionné le fichier 
        ![noelshack.com](https://image.noelshack.com/fichiers/2019/08/5/1550847283-screen-selectsql.png)
    Cliquer sur éxecuter
        ![noelshack.com](https://image.noelshack.com/fichiers/2019/08/5/1550847283-terminus.png)
    Cliquer sur exécuter    

#titre La base de données est en place sur le serveur mysql 


#titre L'utilité de cette base de donnée est la gestion des emprunts dans une bibliothéque 

#titre On crée donc trois table 

    Book : Gére les livres de la bibliothéque
    Member : Gére les membre qui emprunte des livres 
    borrowing : Gére les emprunt de la bibliothéque
    
*********************************    
Book (

    Title -> Titre du livre
)
*********************************  
Member (
    Lastname -> Nom
    Firstname -> Prénom
    mail -> Email
    borrowingId -> ID de l'emprunt
)
*********************************  
Borrowing{
    id -> id du membre
)
*********************************  
 borrowing_book(
 
    borrowing_id -> id de l'emprun
    Book_id -> id du livre
    
)
*********************************  
 
    